
import eng from './translations/eng.json' assert {type: 'json'}
import ukr from './translations/ukr.json' assert {type: 'json'}

const button = document.querySelector(`.btn-container button`)
let activeLang = ''

const setTranslations = (lang) => {
    activeLang = lang
    const translations = activeLang === 'ukr' ? ukr : eng

    for (let key in translations) {
        const elements = document.querySelectorAll(`[data-t-key=${key}]`)

        elements.forEach(el => {
            el.textContent = translations[key]
        })
    }
}
button.addEventListener(`click`, function () {
    if (activeLang === 'ukr') {
        setTranslations('eng')
    } else {
        setTranslations('ukr')
    }
});

setTranslations('ukr')


